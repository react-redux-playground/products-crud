import axios from 'axios';

export default axios.create({
    baseURL: process.env.NODE_URL || 'http://localhost:3002'
});