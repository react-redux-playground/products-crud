import React from 'react';
import NavBar from './Layout/NavBar';
import Content from './Layout/Content';

import history from '../history';
import { Router } from 'react-router-dom';

const App = () => {
    return (
       <div className='app'>
            <Router history={history}>
                <NavBar />
                <Content />
            </Router>
       </div>
    );
}   

export default App;
