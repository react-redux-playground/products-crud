import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchProduct, editProduct } from '../../store/actions/ProductActions'
import ProductForm from './ProductForm';

class ProductEdit extends React.Component{
    componentDidMount() {
        this.props.fetchProduct(this.props.match.params.id);
    }

    onSubmit = (formValues) => {
        console.log(formValues);
        this.props.editProduct(this.props.match.params.id, formValues)
    }

    render() {
        console.log('edit...', this.props)
        if(!this.props.selectedProduct) {
            return(
                <div>Loading...</div>
             )
        }
       
        return(
            <div>
                <h3>Edit Product</h3>
                <ProductForm 
                    onSubmit={this.onSubmit}
                    initialValues=
                        {_.pick(this.props.selectedProduct, 
                            'title', 
                            'description',
                            'image',
                            'price',
                            'sku',
                            'quantity' 
                        )} />
            </div>
        )
        
    }
}

const mapStateToProps = (state, ownProps) => {
    // console.log(state);
    return {selectedProduct: state.product.manipulateProduct}
}

export default connect(
    mapStateToProps, { fetchProduct, editProduct }
)(ProductEdit);