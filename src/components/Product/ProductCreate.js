import React from 'react';
import { connect } from 'react-redux';
import { createProduct } from '../../store/actions/ProductActions';
import { Link } from 'react-router-dom';
import ProductForm from './ProductForm';
import {
    Button,
    Typography,
    Grid
} from '@material-ui/core';



class ProductCreate extends React.Component {
  
    onSubmit = (formValues) => {
        // console.log(formValues);
        this.props.createProduct(formValues);
    }

    render() {
        return(
            <React.Fragment>
                <Grid container direction="row" justify="space-between" alignItems="center">
                    <Typography component="h1" variant="h5">
                        Create Product
                    </Typography>
                    <Button 
                        variant="contained" color="primary" 
                        component={Link} to="/products">
                            Back
                    </Button>
                </Grid>
                <div>
                    <ProductForm onSubmit={this.onSubmit}/>
                </div>

            </React.Fragment>
        )
    }
}

export default connect(  
    null, 
    { createProduct }
)(ProductCreate)