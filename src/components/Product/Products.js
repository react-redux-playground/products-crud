import React from 'react';
import { connect } from 'react-redux';
import { fetchProducts } from '../../store/actions/ProductActions';
import { Link } from 'react-router-dom';
import ProductListTable from './ProductListTable';
import { Button, Grid, Typography } from '@material-ui/core';


class Products extends React.Component {
    componentDidMount() {
        this.props.fetchProducts();
    }

    render() {
        return(
            <div>
                <Grid style={{marginBottom: '1rem'}}
                    container direction="row" justify="space-between" alignItems="center">
                    <Typography component="h1" variant="h5">Products</Typography>
                    <Button component={Link} to="/products/create" 
                            variant="contained" color="primary">
                        Create Product
                    </Button>
                </Grid>
                <Grid container>
                   <ProductListTable products={this.props.products} />
                </Grid>
            </div>
        )
    }
}


const mapStateToProps = state => {
    console.log('state 1111', state);
    return {
        products: state.product.products
    };
}

export default connect(
    mapStateToProps, {fetchProducts}
)(Products)