import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import history from '../../history';
import { deleteProduct } from '../../store/actions/ProductActions';
import ProductDelete from './ProductDelete';
import { EditOutlined, DeleteOutline } from '@material-ui/icons';
import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    CardMedia,
    Button
} from '@material-ui/core';


class ProductTable extends React.Component {
    state = {
        isDialogOpen: false,
        selectedProduct: {}
    };

    handleDelete = (product) => {
        console.log('selected id ', product._id);
        this.setState({isDialogOpen: true});
        this.setState({selectedProduct: product});
    }

    handleOnClose = (value) => {
        this.setState({isDialogOpen: false});
        if(value === 'YES') {
            this.props.deleteProduct(this.state.selectedProduct._id);
        }
        history.push('/products');
    }

    render() {
        return (
           <React.Fragment>
                <Table>
                <TableHead>
                    <TableRow>
                        <TableCell></TableCell>
                        <TableCell>Product Name</TableCell>
                        <TableCell>Price</TableCell>
                        <TableCell>SKU</TableCell>
                        <TableCell>Quantity</TableCell>
                        <TableCell>Actions</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.props.products.map(product => (
                        <TableRow key={product._id}>
                            <TableCell component="th" scope="row">
                            <CardMedia component="img" height="50" width="50"
                                        src={product.image}  
                                        style={{objectFit: 'scale-down', width: 'auto'}} 
                            />   
                        </TableCell>
                            <TableCell>{product.title}</TableCell>
                            <TableCell>$ {product.price}</TableCell>
                            <TableCell>{product.sku}</TableCell>
                            <TableCell>{product.quantity}</TableCell>
                            <TableCell>
                            <div className="actions">
                                <Button variant="outlined"
                                        component={ Link } to={`/products/edit/${product._id}`}>
                                    <EditOutlined color="primary" /> 
                                    <span>Edit</span>
                                </Button>

                                <Button variant="outlined" onClick={()=>{this.handleDelete(product)}}>
                                    <DeleteOutline variant="contained" color="primary"/> 
                                    <span>Delete</span>
                                </Button>
                            </div>
                        </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>

            <ProductDelete 
                open={this.state.isDialogOpen} 
                onClose={this.handleOnClose} 
                selectedProduct={this.state.selectedProduct}/>
           </React.Fragment>
        )
    }
}

export default connect(null, {deleteProduct})(ProductTable);