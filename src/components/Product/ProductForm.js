import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {
    Button, 
    TextField
} from '@material-ui/core';


class ProductForm extends React.Component {
    renderError({ touched, error }) {
        if(touched && error) {
            return(
                <span className='text-danger'>{error}</span>
            )
        } 
    }

    renderMatInput = ({input, label, name, isFocus, type, meta}) => {
        return (
           <div>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id={name}
                    label={label}
                    name={name}
                    autoFocus={isFocus}
                    type={type}
                    {...input}
                />
                {this.renderError(meta)}
            </div>
        )
    }

    onSubmit = (formValues) => {
        // console.log(formValues);
        this.props.onSubmit(formValues);
    }

    render() {
        return(
            <React.Fragment>
                <form className="form" onSubmit={this.props.handleSubmit(this.onSubmit)}>
                    <Field 
                        component={this.renderMatInput}
                        name="title"
                        label="Title"
                        type="text"
                        isFocus={true}
                    />

                    <Field 
                        component={this.renderMatInput}
                        name="description"
                        label="Description"
                        type="text"
                        isFocus={false}
                    />

                    <Field 
                        component={this.renderMatInput}
                        name="image"
                        label="Image"
                        type="text"
                        isFocus={false}
                    />

                    <Field 
                        component={this.renderMatInput}
                        name="price"
                        label="Price"
                        type="text"
                        isFocus={false}
                    />

                    <Field 
                        component={this.renderMatInput}
                        name="sku"
                        label="SKU"
                        type="text"
                        isFocus={false}
                    />

                    <Field 
                        component={this.renderMatInput}
                        name="quantity"
                        label="Quantity"
                        type="number"
                        isFocus={false}
                    />
                    <div className="submitBtn">
                        <Button type="submit" fullWidth variant="contained" color="primary">
                            Submit
                        </Button>
                    </div>
                </form>
            </React.Fragment>
        )
    }
}

const validate = (formValues) => {
    const errors = {};

    if(!formValues.title)
        errors.title = 'Title is required';

    if(!formValues.description)
        errors.description = 'Description is required';

    if(!formValues.price)
        errors.price = 'Price is required';

    return errors;
}

export default reduxForm({
    form: 'productForm',
    validate
})(ProductForm)