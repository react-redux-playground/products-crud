import React from 'react';
import { Paper } from '@material-ui/core';
import ProductTable from './ProductTable';

// export default class ProductListTable extends React.Component {

//     render() {
//         return(
//             <React.Fragment>
//                 <Paper style={{width: '100%'}}>
//                     <ProductTable products={this.props.products}/>
//                 </Paper>
//             </React.Fragment>
//         )
//     }
// }

const ProductListTable = (props) => {
    return (
        <React.Fragment>
            <Paper style={{width: '100%'}}>
                <ProductTable products={props.products}/>
            </Paper>
        </React.Fragment>
    )
}

export default ProductListTable;