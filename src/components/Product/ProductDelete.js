import React from 'react';
import MaterialDialog from '../Shared/MaterialDialog';

class ProductDelete extends React.Component {

    handleOnClose = (value) => {
        this.props.onClose(value);
    }

    render() {
        return(
            <div>
                <MaterialDialog 
                    open={this.props.open} 
                    onClose={this.handleOnClose} 
                    title="Delete Product"
                    description={`Are you sure you want to delete this product: ${this.props.selectedProduct.title}`}
                    actions="double" 
                    actionNegative="NO" 
                    actionPositive="YES"
                />
            </div>
        )
    }
}

export default ProductDelete;