import React from 'react';
import { Route } from 'react-router-dom';

import Products from '../Product/Products';
import ProductCreate from '../Product/ProductCreate';
import ProductEdit from '../Product/ProductEdit';

const Content = () => {
    return (
        <div className='content'>
            <Route exact path="/" component={Products}></Route>
            <Route exact path="/products" component={Products}></Route>
            <Route exact path="/products/create" component={ProductCreate}></Route>
            <Route exact path="/products/edit/:id" component={ProductEdit}></Route>
        </div>
    )
}

export default Content;