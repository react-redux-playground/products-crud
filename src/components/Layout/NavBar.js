import React from 'react';
import { Link } from 'react-router-dom';

import {
    AppBar,
    Toolbar,
    Typography,
    IconButton,
} from '@material-ui/core';
import {
    AccountCircle,
    ShoppingCartOutlined
} from '@material-ui/icons';


const NavBar = () => {
    return (
       <div className="sticky-top">
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6">
                        <div className='navbar-logo fxRow fxAlignCenter'>
                            <Link to="/" className="logo-text">ReactShop</Link>
                        </div>
                    </Typography>
                    
                    <div className='spacer'></div>

                    <IconButton color="inherit">
                        <ShoppingCartOutlined/>
                    </IconButton>

                    <IconButton color="inherit">
                        <AccountCircle />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>  
    )
}

export default NavBar;