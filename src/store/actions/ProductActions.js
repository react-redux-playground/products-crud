import http from '../../services/http'
import history from '../../history';
import {
    FETCH_PRODUCTS,
    FETCH_PRODUCT,
    CREATE_PRODUCT,
    EDIT_PRODUCT,
    DELETE_PRODUCT
} from './types';


export const fetchProducts = () => async dispatch => {
    const response = await http.get('/products');
    console.log('products > ', response.data);

    dispatch({
        type: FETCH_PRODUCTS,
        payload: response.data
    });
}

export const createProduct = (product) => async dispatch => {
    const response = await http.post('/products', product);
    dispatch({
        type: CREATE_PRODUCT,
        payload: response.data
    })

    //do programmatic navigation back to root route if response success
    history.push('/products')
}

export const deleteProduct = (id) => async dispatch => {
    console.log('deleting...')
    const response = await http.delete(`/products/${id}`);
    dispatch({
        type: DELETE_PRODUCT,
        payload: response.data
    });
}

export const fetchProduct = id => async dispatch => {
    const response = await http.get(`/products/${id}`);
    dispatch({
        type: FETCH_PRODUCT,
        payload: response.data
    })
}

export const editProduct = (id, formValues) => async dispatch => {
    console.log(`editing ${id}...`);
    const response = await http.patch(`/products/${id}`, formValues);
    dispatch({
        type: EDIT_PRODUCT,
        payload: response.data
    });

    history.push('/products');
}