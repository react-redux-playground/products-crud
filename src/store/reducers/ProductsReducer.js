import {
    FETCH_PRODUCTS,
    FETCH_PRODUCT,
    CREATE_PRODUCT,
    EDIT_PRODUCT,
    DELETE_PRODUCT
} from '../actions/types'

const initialState = {
    products: [],
    selectedProduct: null,
    manipulateProduct: null,
}

export const ProductsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS: 
            return {
                ...state,
                products: action.payload,
                selectedProduct: null,
                manipulateProduct: null
            }

        case FETCH_PRODUCT:
            return {
                ...state,
                products: state.products,
                selectedProduct: null,
                manipulateProduct: action.payload
            }

        case EDIT_PRODUCT:
            return {
                ...state,
                products: state.products,
                selectedProduct: null,
                manipulateProduct: action.payload
            }

        case CREATE_PRODUCT:
            return {
                ...state,
                products: state.products,
                selectedProduct: null,
                manipulateProduct: action.payload
            }

        case DELETE_PRODUCT: 
            console.log('response ', action.payload);
            return {
                ...state,
                products: state.products.filter(product => product._id !== action.payload),
                selectedProduct: null,
                manipulateProduct: null
            }

        default: 
            return state;
    }
}