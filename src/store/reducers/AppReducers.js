import { combineReducers } from 'redux';
import { reducer as FormReducer } from 'redux-form';

import { ProductsReducer } from './ProductsReducer';

export default combineReducers ({
    form: FormReducer,
    product: ProductsReducer
});
