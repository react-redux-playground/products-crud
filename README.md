A basic application that uses a React Redux to capture and manage products, connecting to NodeJS application written in Express in another repo (nodejs-playgorund/crud-starter), and uses MongoDB to store products information.

<h1>Features</h1>
<ul>
    <li>Display all products in a table</li>
    <li>Create a product</li>
    <li>Edit a product</li>
    <li>Delete a product</li>
</ul>

<p>The application is developed with Material UI, some components include table, form, confirmation dialog, icon, button and text input.</p>

<h1>Screenshots</h1>

![image](/uploads/979661cd54ad625fbe83c0c7ac40d2ab/image.png)

![image](/uploads/b3e6c8d55526664725153bce718b12e9/image.png)

![image](/uploads/5354581f607cd50bb7bf3ec5d2eee3af/image.png)

